## Configure and run

1. Configure `FT3PerformanceTest/Program.cs`
2. `dotnet restore`
3. `dotnet run --project FT3PerformanceTest`
