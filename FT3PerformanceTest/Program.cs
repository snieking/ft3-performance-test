﻿using System.Threading;

namespace FT3PerformanceTest
{
    class Program
    {
        private const int Workers = 1000;
        private const string NodeUrl = "http://localhost/";
        private const string Rid = "F3B52657B194D27498F0C28771249A1CC1A3DB7A16102B2047DC7B4ACAA61DBA";
        
        static void Main(string[] args)
        {
            for (int i = 0; i < Workers; i++)
            {
                new Thread(() => new FT3Worker(NodeUrl, Rid).Start()).Start();
            }
            
            while (true) {}
        }
    }
}