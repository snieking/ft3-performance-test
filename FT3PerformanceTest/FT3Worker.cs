using System;
using System.Threading.Tasks;
using Chromia.Postchain.Ft3;

namespace FT3PerformanceTest
{
    public class FT3Worker : Worker
    {
        private readonly string _nodeUrl;
        private readonly byte[] _rid;

        public FT3Worker(string nodeUrl, string rid)
        {
            Console.WriteLine("Creating worker");
            _nodeUrl = nodeUrl;
            _rid = Util.HexStringToBuffer(rid);
        }
        
        public async Task Start()
        {
            Console.WriteLine("Starting worker");
            var directory = new Directory(_nodeUrl, _rid);
            var blockchain = await Blockchain.Initialize(_rid, directory);

            while (true)
            {
                var user = User.GenerateSingleSigUser();
                var session = blockchain.NewSession(user);

                dynamic[] args = { user.AuthDescriptor.ToGTV() };
                var timeBefore = DateTime.Now;
                await session.Call(new Operation("ft3.dev_register_account", args));
                Console.WriteLine($"Register took: {DateTime.Now-timeBefore}");
            }
        }
    }

    internal class Directory : DirectoryService
    {
        private readonly ChainConnectionInfo _chainConnectionInfo;
        public Directory(string nodeUrl, byte[] rid)
        {
            _chainConnectionInfo = new ChainConnectionInfo(rid, nodeUrl);
        }
        
        public ChainConnectionInfo GetChainConnectionInfo(byte[] id)
        {
            return _chainConnectionInfo;
        }
    }
}