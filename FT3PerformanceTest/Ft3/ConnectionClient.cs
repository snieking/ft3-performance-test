using Chromia.Postchain.Client;
using System.Threading.Tasks;

namespace Chromia.Postchain.Ft3
{
  public class ConnectionClient
  {
    public string ChainUrl;
    public string ChainID;
    public GTXClient Gtx;
    private RESTClient restClient;

    public ConnectionClient(string chainURL, string chainID)
    {
      this.ChainUrl = chainURL;
      this.ChainID = chainID;

      this.restClient = new RESTClient(chainURL, chainID);
      this.Gtx = new GTXClient(this.restClient);
    }

    public int RequestTimeout {
      get => restClient.RequestTimeout;
      set => restClient.RequestTimeout = value;
    }

    public async Task<(T content, PostchainErrorControl control)> Query<T>(string name, params (string name, object content)[] queryObject)
    {
      return await this.Gtx.Query<T>(name, queryObject);
    }
  }
}