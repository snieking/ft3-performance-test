using System;
using System.Linq;
using Chromia.Postchain.Client;
using System.Threading.Tasks;

namespace Chromia.Postchain.Ft3
{
  public class BlockchainSession
  {
    public readonly User User;
    public readonly Blockchain Blockchain;

    public BlockchainSession(User user, Blockchain blockchain)
    {
      User = user;
      Blockchain = blockchain;
    }

    public async Task<Account> GetAccountById(byte[] id)
    {
      return await Account.GetById(id, this);
    }

    public async Task<Account[]> GetAccountsByParticipantId(byte[] id)
    {
      return await Account.GetByParticipantId(id, this);
    }

    public async Task<Account[]> GetAccountsByAuthDescriptorId(byte[] id)
    {
      return await Account.GetByAuthDescriptorId(id, this);
    }

    public async Task<(T content, PostchainErrorControl control)> Query<T>(string name, params (string name, object content)[] queryObject)
    {
      var result = await Blockchain.Query<T>(name, queryObject);
      if (result.control.Error)
      {
        Console.WriteLine($"Error while running query {name}: {result.control.ErrorMessage}");
      }

      return result;
    }

    public async Task<PostchainErrorControl> Call(params Operation[] operations)
    {
      var result = await Blockchain.Call(User, operations);

      if (result.Error)
      {
        Console.WriteLine($"Error sending operations {String.Join(", ", operations.ToList().Select(op => op.Name))}");
      }
      
      return result;
    }
  }
}