using System.Threading.Tasks;

namespace FT3PerformanceTest
{
    public interface Worker
    {
        public Task Start();
    }
}